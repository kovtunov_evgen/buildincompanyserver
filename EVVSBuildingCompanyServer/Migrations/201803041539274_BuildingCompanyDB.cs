namespace EVVSBuildingCompanyServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BuildingCompanyDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContentName",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ContentText",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        ContentName_ID = c.Int(),
                        Language_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContentName", t => t.ContentName_ID)
                .ForeignKey("dbo.Language", t => t.Language_ID)
                .Index(t => t.ContentName_ID)
                .Index(t => t.Language_ID);
            
            CreateTable(
                "dbo.Language",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Locale = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContentText", "Language_ID", "dbo.Language");
            DropForeignKey("dbo.ContentText", "ContentName_ID", "dbo.ContentName");
            DropIndex("dbo.ContentText", new[] { "Language_ID" });
            DropIndex("dbo.ContentText", new[] { "ContentName_ID" });
            DropTable("dbo.Language");
            DropTable("dbo.ContentText");
            DropTable("dbo.ContentName");
        }
    }
}
