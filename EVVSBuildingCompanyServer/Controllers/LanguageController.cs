﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EVVSBuildingCompanyServer.DAL;
using EVVSBuildingCompanyServer.Models;

namespace EVVSBuildingCompanyServer.Controllers
{
    public class LanguageController : ApiController
    {
        public class FomatetContent
        {
            public string lang { get; set; }

            public Dictionary<string, string> content { get; set; }

            public FomatetContent(string lang, Dictionary<string, string> content)
            {
                this.lang = lang;
                this.content = content;
            }
            

        }
        private BuildingCompanyContext db = new BuildingCompanyContext();

        //api/getlanguage?lang={}
        public FomatetContent GetLanguage(string lang)
        {
            try
            {
                var langObj = db.Languages.First(lan => lan.Description == lang);

                return Format(langObj);
            }
            catch
            {
                return new FomatetContent(String.Empty, null);
            }
        }

        // GET: api/Language
        public List<FomatetContent> GetLanguages()
        {
            var result = new List<FomatetContent>();

            foreach (var lang in db.Languages.ToList())
            {
                result.Add(Format(lang));
            }

            return result;
        }

        private FomatetContent Format(Language lang)
        {
            var content = new Dictionary<string, string>();

            foreach (var contentText in lang.ContentTexts.ToList())
            {
                content.Add(contentText.ContentName.Name, contentText.Text);
            }

            return new FomatetContent(lang.Description, content);
        }

        // GET: api/Language/5
        [ResponseType(typeof(Language))]
        public IHttpActionResult GetLanguage(int id)
        {
            Language language = db.Languages.Find(id);
            if (language == null)
            {
                return NotFound();
            }

            return Ok(language);
        }

        // PUT: api/Language/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLanguage(int id, Language language)
        {


            if (id != language.ID)
            {
                return BadRequest();
            }

            db.Entry(language).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LanguageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Language
        [ResponseType(typeof(Language))]
        public IHttpActionResult PostLanguage(Language language)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Languages.Add(language);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = language.ID }, language);
        }

        // DELETE: api/Language/5
        [ResponseType(typeof(Language))]
        public IHttpActionResult DeleteLanguage(int id)
        {
            Language language = db.Languages.Find(id);
            if (language == null)
            {
                return NotFound();
            }

            db.Languages.Remove(language);
            db.SaveChanges();

            return Ok(language);
        }

       

        private bool LanguageExists(int id)
        {
            return db.Languages.Count(e => e.ID == id) > 0;
        }
    }
}