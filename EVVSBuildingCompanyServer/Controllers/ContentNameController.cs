﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EVVSBuildingCompanyServer.DAL;
using EVVSBuildingCompanyServer.Models;

namespace EVVSBuildingCompanyServer.Controllers
{
    public class ContentNameController : ApiController
    {
       

        private BuildingCompanyContext db = new BuildingCompanyContext();

        // GET: api/ContentName
        public IQueryable<ContentName> GetContentNames()
        {
            return db.ContentNames;
        }

        // GET: api/ContentName/5
        [ResponseType(typeof(ContentName))]
        public IHttpActionResult GetContentName(int id)
        {
            ContentName contentName = db.ContentNames.Find(id);
            if (contentName == null)
            {
                return NotFound();
            }

            return Ok(contentName);
        }

        // PUT: api/ContentName/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutContentName(int id, ContentName contentName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contentName.ID)
            {
                return BadRequest();
            }

            db.Entry(contentName).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContentNameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ContentName
        [ResponseType(typeof(ContentName))]
        public IHttpActionResult PostContentName(ContentName contentName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ContentNames.Add(contentName);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = contentName.ID }, contentName);
        }

        // DELETE: api/ContentName/5
        [ResponseType(typeof(ContentName))]
        public IHttpActionResult DeleteContentName(int id)
        {
            ContentName contentName = db.ContentNames.Find(id);
            if (contentName == null)
            {
                return NotFound();
            }

            db.ContentNames.Remove(contentName);
            db.SaveChanges();

            return Ok(contentName);
        }

       

        private bool ContentNameExists(int id)
        {
            return db.ContentNames.Count(e => e.ID == id) > 0;
        }
    }
}