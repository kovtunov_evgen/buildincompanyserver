﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http;
using System.Web.Http.Description;
using EVVSBuildingCompanyServer.DAL;
using EVVSBuildingCompanyServer.Models;

namespace EVVSBuildingCompanyServer.Controllers
{
    public class AdminController : ApiController
    {
        private static BuildingCompanyContext db = new BuildingCompanyContext();
        private static readonly IAdminTableFactory tableFactory=new AdminTableFactory(db);


        public interface IAdminTableFactory
        {
            AdminTable Construct();
        }

        public class AdminTableFactory : IAdminTableFactory
        {
            private BuildingCompanyContext adminContext;

            public AdminTableFactory(BuildingCompanyContext adminContext)
            {
                this.adminContext = adminContext;
            }

            public AdminTable Construct()
            {
                var table = new AdminTable();
                table.Rows = new List<AdminTableRow>();

                var languages = adminContext.Languages.ToList();

                table.Languages= new List<LanguageAdapter>();
                foreach (var language in languages)
                {
                    table.Languages.Add( new LanguageAdapter(language));
                }

                foreach (var contentName in adminContext.ContentNames.ToList())
                {
                    var row = new AdminTableRow();
                    row.ContentName = new ContentNameAdapter(contentName);

                    var texts = new List<ContentTextAdapter>();
                    foreach (var language in languages)
                    {
                        try
                        {
                            texts.Add(
                                new ContentTextAdapter(adminContext
                                    .ContentTexts
                                    .ToList()
                                    .First(text =>
                                        text.ContentName == contentName && text.Language == language))
                            );
                        }
                        catch
                        {
                            texts.Add(
                                new ContentTextAdapter());
                        }
                    }

                    row.ContentTexts = texts;
                    table.Rows.Add(row);
                }

                return table;
            }

        }

        [DataContract]
        public class ContentNameAdapter
        {
            [DataMember] public int ID { get; set; }
            [DataMember] public string Name { get; set; }

            public ContentNameAdapter(ContentName name)
            {
                ID = name.ID;
                Name = name.Name;
            }

        }

        [DataContract]
        public class LanguageAdapter
        {
            [DataMember] public int ID { get; set; }
            [DataMember] public string Description { get; set; }
            [DataMember] public string Locale { get; set; }

            public LanguageAdapter(Language instance)
            {
                ID = instance.ID;
                Description = instance.Description;
                Locale = instance.Locale;
            }
        }
        
        [DataContract]
        public class ContentTextAdapter
        {
            [DataMember] public int ID { get; set; }
            [DataMember] public string Text { get; set; }

            public ContentTextAdapter(ContentText instance)
            {
                ID = instance.ID;
                Text = instance.Text;
            }

            public ContentTextAdapter()
            {
                ID = 0;
                Text = string.Empty;
            }
        }

        [DataContract]
        public class AdminTableRow
        {
            [DataMember] public ContentNameAdapter ContentName { get; set; }
            [DataMember] public List<ContentTextAdapter> ContentTexts { get; set; }
        }

        [DataContract]
        public class AdminTable
        {
            [DataMember] public List<AdminTableRow> Rows { get; set; }
            [DataMember] public List<LanguageAdapter> Languages { get; set; }
        }
        
        public AdminTable GetData()
        {
            return tableFactory.Construct();
        }



        /*// GET: api/Admin
        [ResponseType(typeof(ContentName))]
        public IQueryable<ContentName> GetContentNames()
        {
            return db.ContentNames;
        }**/
        
        // GET: api/Admin/5
        //[ResponseType(typeof(ContentName))]
        //public List<ContentText> GetData()
        //{
        //    List<ContentText> contentText = new List<ContentText>();
        //    contentText = db.ContentTexts.Include(s => s.ContentName).Include(s => s.Language).ToList();

        //    return contentText;
        //}

        // PUT: api/Admin/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutContentName(int id, ContentName contentName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contentName.ID)
            {
                return BadRequest();
            }

            db.Entry(contentName).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContentNameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Admin
        [ResponseType(typeof(ContentName))]
        public IHttpActionResult PostContentName(ContentName contentName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ContentNames.Add(contentName);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = contentName.ID }, contentName);
        }

        // DELETE: api/Admin/5
        [ResponseType(typeof(ContentName))]
        public IHttpActionResult DeleteContentName(int id)
        {
            ContentName contentName = db.ContentNames.Find(id);
            if (contentName == null)
            {
                return NotFound();
            }

            db.ContentNames.Remove(contentName);
            db.SaveChanges();

            return Ok(contentName);
        }

       

        private bool ContentNameExists(int id)
        {
            return db.ContentNames.Count(e => e.ID == id) > 0;
        }
    }
}