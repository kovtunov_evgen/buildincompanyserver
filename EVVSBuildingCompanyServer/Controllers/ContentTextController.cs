﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EVVSBuildingCompanyServer.DAL;
using EVVSBuildingCompanyServer.Models;

namespace EVVSBuildingCompanyServer.Controllers
{
    public class ContentTextController : ApiController
    {
        

        private BuildingCompanyContext db = new BuildingCompanyContext();

        // GET: api/ContentText
        public IQueryable<ContentText> GetContentTexts()
        {
            return db.ContentTexts;
        }

        // GET: api/ContentText/5
        [ResponseType(typeof(ContentText))]
        public IHttpActionResult GetContentText(int id)
        {
            ContentText contentText = db.ContentTexts.Find(id);
            if (contentText == null)
            {
                return NotFound();
            }

            return Ok(contentText);
        }

        // PUT: api/ContentText/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutContentText(int id, ContentText contentText)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contentText.ID)
            {
                return BadRequest();
            }

            db.Entry(contentText).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContentTextExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ContentText
        [ResponseType(typeof(ContentText))]
        public IHttpActionResult PostContentText(ContentText contentText)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ContentTexts.Add(contentText);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = contentText.ID }, contentText);
        }

        // DELETE: api/ContentText/5
        [ResponseType(typeof(ContentText))]
        public IHttpActionResult DeleteContentText(int id)
        {
            ContentText contentText = db.ContentTexts.Find(id);
            if (contentText == null)
            {
                return NotFound();
            }

            db.ContentTexts.Remove(contentText);
            db.SaveChanges();

            return Ok(contentText);
        }

       

        private bool ContentTextExists(int id)
        {
            return db.ContentTexts.Count(e => e.ID == id) > 0;
        }
    }
}