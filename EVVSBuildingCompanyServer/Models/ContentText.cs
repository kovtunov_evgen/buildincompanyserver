﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EVVSBuildingCompanyServer.Models
{
    public class ContentText
    {
        public int ID { get; set; }
       
        public string Text { get; set; }

        public virtual ContentName ContentName { get; set; }
        public virtual Language Language { get; set; }
    }
}