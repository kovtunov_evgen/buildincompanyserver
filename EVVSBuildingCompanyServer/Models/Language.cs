﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EVVSBuildingCompanyServer.Models
{
    public class Language
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Locale { get; set; }

        public virtual ICollection<ContentText> ContentTexts { get; set; }
    }
}