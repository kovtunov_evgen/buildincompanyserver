﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EVVSBuildingCompanyServer.Models
{
    public class ContentName
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ContentText> ContentTexts { get; set; }
    }
}