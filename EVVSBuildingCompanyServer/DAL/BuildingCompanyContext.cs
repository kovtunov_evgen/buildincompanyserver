﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EVVSBuildingCompanyServer.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
namespace EVVSBuildingCompanyServer.DAL
{
    public class BuildingCompanyContext : DbContext
    {
        public BuildingCompanyContext() : base("BuildingCompanyContext")
        {
        }

        public DbSet<Language> Languages { get; set; }
        public DbSet<ContentName> ContentNames { get; set; }
        public DbSet<ContentText> ContentTexts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}